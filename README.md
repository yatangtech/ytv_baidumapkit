# YTV_BaiduMapKit

[![CI Status](http://img.shields.io/travis/cleexiang/YTV_BaiduMapKit.svg?style=flat)](https://travis-ci.org/cleexiang/YTV_BaiduMapKit)
[![Version](https://img.shields.io/cocoapods/v/YTV_BaiduMapKit.svg?style=flat)](http://cocoapods.org/pods/YTV_BaiduMapKit)
[![License](https://img.shields.io/cocoapods/l/YTV_BaiduMapKit.svg?style=flat)](http://cocoapods.org/pods/YTV_BaiduMapKit)
[![Platform](https://img.shields.io/cocoapods/p/YTV_BaiduMapKit.svg?style=flat)](http://cocoapods.org/pods/YTV_BaiduMapKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YTV_BaiduMapKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YTV_BaiduMapKit"
```

## Author

cleexiang, cleexiang@126.com

## License

YTV_BaiduMapKit is available under the MIT license. See the LICENSE file for more info.
